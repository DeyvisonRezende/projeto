function loadPage() {
    this.mostraDados();
}

function mostraDados() {
    const request = verificaRequest();
 
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {

            dados = JSON.parse(request.responseText);
            let pag = 0;

            for (let i = 0; i < dados[0].data.recommendation.length; i++) {
                const item = dados[0].data.recommendation[i];
                const element = `
                    <li>
                        ${i === 0 ? `<span class="titulo">Você visitou:</span>` :
                        i === 1 ? `<span class="titulo">e talvez se interesse por:</span>` : ''}
                        <div class="card">
                            <div class="imagem">
                                <img src="${item.imageName}" alt="${item.name.slice(0, 15) + '...'}">
                            </div>
                            <div class="nome">${item.name}</div>
                            ${item.oldPrice ? `<div>
                                <span>De:</span>
                                <span>${item.oldPrice}</span>
                            </div>` : ''}
                            <div class="preco">
                                <span>Por:</span>
                                <span>${item.price}</span>
                            </div>
                            <div class="parcelamento">
                                <span>${item.productInfo.paymentConditions}</span>
                            </div>
                            <button type="button">
                                adicionar ao carrinho
                                <i class="material-icons">add_shopping_cart</i>
                            </button>
                        </div>
                    </li>`;
                if (i % 4 === 0) {
                    document.getElementsByClassName('pagina')[0].insertAdjacentHTML(
                        'beforeend', 
                        `<ul id="pag-${pag}"></ul>`
                    );
                    document.getElementsByClassName('paginacao')[0].insertAdjacentHTML(
                        'beforeend', 
                        `<a href="#pag-${pag}"><div onclick="selecionarPagina(this)"
                            ${pag === 0 ? `class="selecionado"` : ''}></div></a>`
                    );
                    pag += 1;
                }
                document.getElementsByTagName('ul')[pag - 1].insertAdjacentHTML('beforeend', element);
            }
            window.location = "#pag-0";
        }
    };
    request.open("GET", "products.json", true);
    request.send();
}

function verificaRequest() {
     return req = (window.XMLHttpRequest) ? new XMLHttpRequest() :
         new ActiveXObject('Microsoft.XMLHTTP');
}

function selecionarPagina(element) {
    document.getElementsByClassName('selecionado')[0].classList.remove('selecionado');
    element.classList.add('selecionado');
}

window.onload = loadPage;