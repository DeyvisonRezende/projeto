# Frontend Test

Widget de prateleira de cross-sell utilizando HTML e CSS.

## Como executar

- Será necessário ter instalado algum programa que emule um servidor em seu computador, recomendo WampServer ou XAMPP.

- Após instalação, basta colar essa pasta dentro do servidor, como por exemplo no XAMPP a pasta deverá ser colada dentro do caminho C:\xampp\htdocs.

- Feito isso, basta executar o servidor e acessar em seu navegador o http://localhost/ colocando o nome pasta do projeto após o caminho especificado.
